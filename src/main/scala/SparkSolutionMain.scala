import common.DataMsg
import interfaces.Spark
import io.circe.parser.decode
import org.apache.spark.SparkConf
import spark.{Classifier, DataGenerator, ModelUpdateGenerator}
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import org.apache.spark.sql.Dataset
import spark.UnifiedData

object SparkSolutionMain extends App {

	val sparkConf = new SparkConf().setAppName("RiskIdent")
	implicit val spark = Spark.sparkSession(sparkConf)

	import spark.implicits._

	val partitions = 4

	val dataStream: Dataset[UnifiedData] = spark
		.readStream
		.format("kafka")
		.option("kafka.bootstrap.servers", "localhost:9092")
		.option("subscribe", "points")
		.load()
		.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
		.map(r => decode[DataMsg](r.getAs[String]("value")).right.get)
		.map(d => UnifiedData(d.id % partitions, null, d))

	val updateModelStream = ModelUpdateGenerator.get(dataStream, 4)

	val dataControlStream = dataStream union (updateModelStream)

	dataControlStream
		.writeStream
		.outputMode("Update")
		.format("console")
		.start()

	val classificationStream = Classifier.classify(dataControlStream)

	val output = classificationStream
		.writeStream
		.outputMode("Update")
		.format("console")
		.start()

	output.awaitTermination()
}
