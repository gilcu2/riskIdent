import common.DataMsg
import interfaces.Spark
import org.apache.spark.SparkConf
import spark.{Classifier, DataGenerator, ModelUpdateGenerator}
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

object SparkKafkaConsumer extends App {

	val sparkConf = new SparkConf().setAppName("PubNative")
	implicit val spark = Spark.sparkSession(sparkConf)

	import spark.implicits._

	val pointsSource = spark
		.readStream
		.format("kafka")
		.option("kafka.bootstrap.servers", "localhost:9092")
		.option("subscribe", "points")
		.load()
	val df1 = pointsSource.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
		.map(r => decode[DataMsg](r.getAs[String]("value")).right.get)


	val output = df1
		.writeStream
		.outputMode("Update")
		.format("console")
		.start()

	output.awaitTermination()

}
