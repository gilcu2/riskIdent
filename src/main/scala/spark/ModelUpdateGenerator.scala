package spark

import common.ModelParametersValues._
import org.apache.spark.sql.{Dataset, SparkSession}

object ModelUpdateGenerator {

	def get(points: Dataset[UnifiedData], partitions: Int)(implicit spark: SparkSession): Dataset[UnifiedData] = {
		import spark.implicits._

		points
			.flatMap(r => {
				val sequeceIndex = r.data.id
				if (sequeceIndex % 100 == 0) {
					val cycleIndex = (sequeceIndex / 100) % 5
					val modelParameter = givenModelParameters(cycleIndex.toInt)
					(0 until partitions).map(i => UnifiedData(i, modelParameter, null))
				}
				else
					Seq()
			})
	}

}
