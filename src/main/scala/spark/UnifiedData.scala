package spark

import common.{ClassificationResult, DataMsg, UpdateModelMsg}

case class UnifiedData(id: Long, updateModel: UpdateModelMsg, data: DataMsg)

case class UnifiedOutput(results: ClassificationResult, accuracy: Double)

case class UnifiedState(model: UpdateModelMsg, nClassified: Long, okClassified: Long)