package spark

import common.{ClassificationResult, LogisticRegression, ModelParameters, UpdateModelMsg}
import org.apache.spark.sql.streaming.{GroupState, GroupStateTimeout, OutputMode}
import org.apache.spark.sql.{Dataset, SparkSession}

object Classifier {

	def classify(ds: Dataset[UnifiedData])(implicit spark: SparkSession)
	: Dataset[UnifiedOutput] = {
		import spark.implicits._

		ds.groupByKey(_.id)
			.flatMapGroupsWithState(OutputMode.Update,
				timeoutConf = GroupStateTimeout.NoTimeout)((id, it, state: GroupState[UnifiedState]) => {
				val seq = it.toSeq

				val (points, modelUpdates) = seq.partition(_.updateModel == null)

				val lastUpdate: UnifiedState = getUpdatedState(state, modelUpdates)

				val logisticRegression = new LogisticRegression(lastUpdate.model.parameters)
				var nClassified = lastUpdate.nClassified
				var okClassified = lastUpdate.okClassified

				val results = points.map(o => {
					val data = o.data
					val cla = logisticRegression.classify(data.input)
					nClassified += 1
					if (cla == data.label)
						okClassified += 1
					val accuracy = okClassified.toDouble / nClassified

					UnifiedOutput(ClassificationResult(data.id, cla, lastUpdate.model.modelId), accuracy)
				}).toIterator

				state.update(lastUpdate.copy(nClassified = nClassified, okClassified = okClassified))
				results

			})

	}

	private def getUpdatedState(state: GroupState[UnifiedState], modelUpdates: Seq[UnifiedData]) = {
		val previousState = state.getOption.getOrElse(UnifiedState(UpdateModelMsg(0,
			ModelParameters(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)), 0, 0))

		val lastUpdate: UnifiedState = if (modelUpdates.nonEmpty) {
			val lastModelUpdate = modelUpdates.reverse.head.updateModel
			UnifiedState(lastModelUpdate, previousState.nClassified, previousState.okClassified)
		} else
			previousState
		lastUpdate
	}
}
