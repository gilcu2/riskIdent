import interfaces.Spark
import org.apache.spark.SparkConf
import spark.{DataGenerator, ModelUpdateGenerator}
import io.circe._
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._

case class KafkaRegister(key: String, value: String)

object SparkKafkaPointProducer extends App {

	val sparkConf = new SparkConf().setAppName("SparkProducer")
	implicit val spark = Spark.sparkSession(sparkConf)

	import spark.implicits._

	val sourcePoints = DataGenerator.get(5, 4)
		.map(d => KafkaRegister(d.id.toString, d.asJson.toString()))

	val kafkaPointStream = sourcePoints
		.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
		.writeStream
		.format("kafka")
		.option("kafka.bootstrap.servers", "localhost:9092")
		.option("checkpointLocation", "/tmp/checkpoint")
		.option("topic", "points")
		.start()

	kafkaPointStream.awaitTermination()
}
