package spark

import org.apache.spark.sql.streaming.Trigger
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import testutils.SparkSessionTestWrapper

class DataGeneratorTest extends AnyFlatSpec with Matchers with SparkSessionTestWrapper {

	implicit val sparkSession = spark

	import spark.implicits._

	"DataGenerator" should "generate a sequence of points" in {

		val points = DataGenerator.mapToDataPoints(Seq(1l, 2l, 3l).toDF).collect()
		points.map(_.id) should be(Seq(1, 2, 3))

	}

}
