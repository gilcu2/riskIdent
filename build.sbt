
name := "riskIdent"

version := "0.1"

scalaVersion := "2.12.8"

val AkkaVersion = "2.6.8"
val sparkV = "2.4.5"
val circeVersion = "0.12.3"

libraryDependencies ++= Seq(

	"io.circe" %% "circe-core" % circeVersion,
	"io.circe" %% "circe-generic" % circeVersion,
	"io.circe" %% "circe-parser" % circeVersion,

	"com.typesafe.akka" %% "akka-actor" % AkkaVersion,
	"com.typesafe.akka" %% "akka-stream" % AkkaVersion,

	"org.apache.spark" %% "spark-sql" % sparkV % "provided",
	"org.apache.spark" %% "spark-streaming-kafka-0-10" % sparkV,

	"com.typesafe.akka" %% "akka-testkit" % AkkaVersion % Test,
	"org.scalatest" %% "scalatest" % "3.2.1" % Test
)

mainClass in(Compile, run) := Some("SparkSolutionMain")

test in assembly := {}

assemblyJarName in assembly := "RiskIdent.jar"

assemblyMergeStrategy in assembly := {
	//  case PathList("org", "apache", "spark", "unused", "UnusedStubClass.class") => MergeStrategy.first
	case PathList("META-INF", xs@_*) => MergeStrategy.discard
	case x => MergeStrategy.first
}

assemblyShadeRules in assembly := Seq(
	ShadeRule.rename("org.slf4j.**" -> "shaded.@1").inAll
)
